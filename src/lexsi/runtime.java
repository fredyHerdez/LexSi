/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexsi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

/**
 *
 * @author fredy
 */
public class runtime {

    /**
     * Desde esta clase se traduce todo el código fuente a código java. Después
     * se crear un archivo .java en el que va a alojar el nuevo código y se
     * compila y se ejecuta.
     *
     * Se manejan las fases: ANALISIS SEMANTICO CREACIÓN DE LENGUAJE INTERMEDIO
     * COMPILACION EJECUCION
     */
    MyObj obj;
    String out;
    String out_e;
    String clase_st;
    URI source_uri;
    String out_code;

    public void instObj(Object o) {
        obj = new MyObj(o.toString());
        out = String.format("EJECUCIÓN:\n    Clase %s:\n", o.toString());
    }

    public String terminar(Object pr, Object cn) {
        clase_st = String.format(
                "package user;\n\npublic class %s \n%s\n\n",
                cn.toString(),
                pr.toString()
        );
        return clase_st;
    }

    /*
    Extras
     */
    public Object expresionIDFilter(Object id) throws Exception {
        if (obj.existsVar(id)) {
            return id;
        }
        report_error(String.format(
                "Variable %s no declaradao",
                id.toString()),
                null
        );
        throw new Exception();
    }

    /*
    Traducir todo
     */
    public String transPrint(Object lit) {
        return String.format("System.out.println(%s)", lit.toString());
    }

    public String transCompare(Object la, Object lb, Object op)
            throws Exception {
        String code;
        /*
        Detectar el tipo de dato de la variable y de ahi entonces hacer lo de 
        equals o los operadores
         */

        try {
            Integer va = Integer.parseInt(la.toString());
            Integer vb = Integer.parseInt(lb.toString());
            String s = getCompareOp(op);
            code = String.format("%d%s%d", va, s, vb);
            return code;
        } catch (NumberFormatException e) {
            /*
            Si se ingresa un valor que no es entero, enteonces es una cadena. 
            Se verifica que no sea una literal string o o un valor true
             */
            String v = la.toString();
            String ta = obj.getVarType(la);
            String tb = obj.getVarType(lb);

            if (!ta.equals(tb)) {
                report_error(
                        "No se pueden compara valores que no son del mismo"
                        + "tipo", null);
                throw new Exception();
            }

            if (v.indexOf("\"") == 0
                    && v.indexOf("\"") == (v.substring(1).length() - 1)) {
                /*
                Se ingreso una cadena, por lo tanto se compara con el equals
                 */
                String s;
                switch (op.toString()) {
                    case "==":
                        return String.format(
                                "%s.equals(%s)",
                                la.toString(),
                                lb.toString()
                        );
                    case "!=":
                        return String.format(
                                "!%s.equals(%s)",
                                la.toString(),
                                lb.toString()
                        );
                    default:
                        report_error(
                                "Operador booleano invalido para el tipo de "
                                + "dato especificado o no reconocido.", null
                        );
                        throw new Exception();
                }

            } else {
                /*
                Se ingreso un identificador, por lo tanto, se procede a 
                obtener el valor de la variable
                 */

                switch (ta) {
                    case "STRING":
                        return String.format(
                                "%s.equals(%s)",
                                la.toString(),
                                lb.toString()
                        );
                    case "INT":
                        return String.format(
                                "%s %s %s",
                                la.toString(),
                                getCompareOp(op),
                                lb.toString()
                        );
                }
                return null;
            }
        }
    }

    public String getCompareOp(Object op) throws Exception {
        /*
        Obtener el valor string del operador ingresado en la funcion comparar
         */
        String s = "";
        switch (op.toString()) {
            case "<":
                s = "<";
                break;
            case ">":
                s = ">";
                break;
            case "<=":
                s = "<=";
                break;
            case ">=":
                s = ">=";
                break;
            case "!=":
                s = "!=";
                break;
            case "==":
                s = "==";
                break;
            default:
                report_error("Operador booleano no reconocido.", null);
                throw new Exception();
        }
        return s;
    }

    public String transAsign(Object id, Object exp) throws Exception {
        /*
        Traducir una sentencia de asginacion al lenguaje java
        Cuando se le asigna un valor a una variable ya declarada
         */
        if (obj.existsVar(id)) {
            String tv = obj.getVarType(id);
            String v = exp.toString();
            if (tv.equals("STRING")) {
                if (v.indexOf("\"") == 0
                        && v.substring(1).indexOf("\"")
                        == (v.substring(1).length() - 1)) {
                    obj.updateVar(id, exp);
                    return String.format(
                            "%s = %s", id.toString(),
                            exp.toString()
                    );
                } else {
                    report_error(
                            "El valor dado es incompatible con el tipo de "
                            + "dato STRING.", null
                    );
                    throw new Exception();
                }
            } else if (tv.toString().equals("BOOL")) {
                switch (exp.toString()) {
                    case "TRUE":
                        obj.updateVar(id, exp);
                        return String.format("%s = true", id.toString());
                    case "FALSE":
                        obj.updateVar(id, exp);
                        return String.format("%s = false", id.toString());
                    default:
                        report_error(
                                "El valor dado es incompatible con el tipo de"
                                + " dato BOOL.", null
                        );
                        throw new Exception();
                }
            } else if (tv.toString().equals("INT")) {
                try {
                    if (v.indexOf("(") == 0
                            && v.substring(1).indexOf(")")
                            == (v.substring(1).length() - 1)) {
                        obj.updateVar(id, exp);
                        return String.format(
                                "%s = %s",
                                id.toString(),
                                v.substring(1, v.length() - 1));
                    } else {
                        Object iv = Integer.parseInt(v);
                        obj.updateVar(id, exp);
                        return String.format(
                                "%s = %s",
                                id.toString(),
                                Integer.parseInt(v)
                        );
                    }

                } catch (NumberFormatException e) {
                    
                    report_error(
                            "El valor dado es incompatible con el tipo de "
                            + "dato INT.", null
                    );
                    throw new Exception();
                }
            } else {
                report_error(
                        "Ha sucedido un error en la asignación de la variable.",
                        null
                );
                throw new Exception();
            }
        } else {
            report_error(
                    String.format(
                            "La variable %s no ha sido declarada",
                            id.toString()
                    ),
                    null
            );
            throw new Exception();
        }
    }

    public String transAsign(Object id, Object exp, Object tv)
            throws Exception {
        /*
        Traducir una sentencia de asignación al lenguaje java
        Cuando se declara una variable nueva
         */

        if (obj.existsVar(id)) {
            report_error(String.format(
                    "La variable %s ya ha sido declarada",
                    id.toString()
            ), null);
            throw new Exception();
        }
        String v = exp.toString();
        if (tv.toString().equals("STRING")) {
            if (v.indexOf("\"") == 0
                    && v.substring(1).indexOf("\"")
                    == (v.substring(1).length() - 1)) {
                obj.setVar(id, exp, tv);
                return String.format(
                        "String %s = %s",
                        id.toString(),
                        exp.toString());
            } else {
                report_error("El valor dado es incompatible con el tipo de "
                        + "dato STRING.", null);
                throw new Exception();
            }
        } else if (tv.toString().equals("BOOL")) {
            switch (exp.toString()) {
                case "TRUE":
                    obj.setVar(id, exp, tv);
                    return String.format(
                            "Boolean %s = true",
                            id.toString()
                    );
                case "FALSE":
                    obj.setVar(id, exp, tv);
                    return String.format(
                            "Boolean %s = false",
                            id.toString()
                    );
                default:
                    report_error(
                            "El valor dado es incompatible con el tipo de "
                            + "dato BOOL.", null
                    );
                    throw new Exception();
            }
        } else if (tv.toString().equals("INT")) {
            try {
                if (v.indexOf("(") == 0
                        && v.substring(1).indexOf(")")
                        == (v.substring(1).length() - 1)) {
                    obj.setVar(id, exp, tv);
                    return String.format(
                            "Integer %s = %s",
                            id.toString(),
                            v.substring(1, v.length() - 1));
                } else {
                    Object iv = Integer.parseInt(exp.toString());
                    obj.setVar(id, exp, tv);
                    return String.format(
                            "Integer %s = %s",
                            id.toString(),
                            Integer.parseInt(exp.toString())
                    );
                }

            } catch (NumberFormatException e) {
                report_error("El valor dado es incompatible con el tipo de "
                        + "dato INT.", null);
                throw new Exception();
            }
        } else {
            report_error("Tipo de dato especificado no aceptado.", null);
            throw new Exception();
        }
    }

    public String transCuerpoFunc(Object c) {
        return String.format("{\n\t%s\n}", c.toString());
    }

    public String transExpArit(Object ea, Object o, Object eb) 
            throws Exception {
        /**
         * Formatear una expresión aritmética. Si es un IDENT, checar que sea de
         * tipo entero y si no, entonces solo pasar el literal entero como
         * parametro
         */
        if(getLitExp(ea) && getLitExp(eb)){
            return String.format(
                    "(%s %s %s)", 
                    ea.toString(), 
                    o.toString(), 
                    eb.toString()
            );
        }else{
            throw new Exception();
        }
    }
    
    public boolean getLitExp(Object l){
        /*
        Funcion paa checar si la expresion ingresada para una expresion
        aritmética es un entero o un identificador.
        */
        try {
            /**
             * Provocar el error para saber si es entero
             */
            Integer.parseInt(l.toString());
            return true;
        } catch (NumberFormatException e) {
            /**
             * Si se dispara el error, entonces verificar que la variable
             * ya esta declarada y asignada y su valor sea de tipo entero
             */
            if(obj.existsVar(l)){
                if(obj.getVarType(l).equals("INT")){
                    return true;
                }else{
                    report_error(
                            String.format("El valor ingresado %s para la "
                                    + "expresion aritmética no es un valor "
                                    + "entero", l.toString()),
                            null
                    );
                    return false;
                }
            }else{
                report_error(
                        String.format(
                                "La variable %s no esta declarada",
                                l.toString()
                        ), 
                        null
                );
                return false;
            }
        }
    }

    /*
    Generar salida del analizador léxico
    -------------------------------------------------
     */
    public void report_error(String message, Object info) {
        /*
        Si sucede algun error, entonces mandar a imprimirlo en la salida del
        compilador
         */
        if (out_e == null) {
            out_e = "";
        }
        StringBuffer m = new StringBuffer("Error");
        if (info instanceof java_cup.runtime.Symbol) {
            java_cup.runtime.Symbol s = ((java_cup.runtime.Symbol) info);
            if (s.left >= 0) {
                m.append(" en linea " + (s.left + 1));
                if (s.right >= 0) {
                    m.append(", y columna " + (s.right + 1));
                }
            }
        }
        m.append(" : " + message);
        out_e += "->" + m;
        System.err.println("Mensaje: " + message);
        System.err.println(m);
    }

    public void print_something(Object o) {
        /*
        Mandar a imprimir cualquier mensaje en la salida del compilador.
         */
        out += String.format("\n    ->%s", o.toString());
    }

    /*
    Generacion del archivo .java
     */
    public boolean genJavaSourceCode() {
        /*
        Escribir todo el codigo traducido en un archivo .java
         */
        try {
            String fileName = String.format(
                    "src/user/%s.java", 
                    obj.getName()
            );
            URL current_loc = LexSi.class.
                    getProtectionDomain().
                    getCodeSource().
                    getLocation();
            source_uri = current_loc.
                    toURI().
                    resolve("..").
                    resolve("..").
                    resolve(fileName);
            File java_source_file = new File(source_uri);
            FileWriter fw = new FileWriter(java_source_file);
            fw.write(clase_st);
            fw.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(runtime.class.getName()).log(
                    Level.SEVERE, 
                    null, ex
            );
        } catch (URISyntaxException ex) {
            Logger.getLogger(runtime.class.getName()).log(
                    Level.SEVERE, 
                    null, 
                    ex
            );
        }
        return false;
    }

    /**
     * Compilar archivo java
     */
    public void compileJavaSource() {
        /**
         * Compilar todo el archivo .java generado anteriormente, para esto, ya
         * tiene que estar generado el archivo fuente. El resultado final es un
         * archivo con terminacion .class.
         */
        try {
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            compiler.run(null, null, null, source_uri.getPath());
        } catch (Exception e) {
            Logger.getLogger(
                    runtime.class.getName()
            ).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Instanciar y correr la clase compilada anteriormente.
     *
     * @return
     */
    public boolean runClass() {
        /**
         * Una vez compilada la clase, entonces correrla!!
         */
        try {
            URL current_loc = LexSi.class.getProtectionDomain().getCodeSource().getLocation();
            URI loc_uri = current_loc.toURI().resolve("..").resolve("..").resolve("src");
            
            Process p = Runtime.getRuntime().exec(
                    String.format(
                            "java -cp %s "
                                    + "user.%s",
                            loc_uri.getPath(),
                            obj.getName()
                    )
            );
            p.waitFor();
            InputStream in = p.getInputStream();
//            InputStream err = p.getErrorStream();

            byte b[]=new byte[in.available()];
            in.read(b,0,b.length);
            out_code = new String(b);

//            byte c[]=new byte[err.available()];
//            err.read(c,0,c.length);
//            System.out.println(new String(c));
//            URLClassLoader classLoader;
//            classLoader = URLClassLoader.newInstance(
//                    new URL[]{source_uri.toURL()}
//            );
//            Class<?> cls = Class.forName(
//                    String.format("user.%s", obj.getName()), 
//                    true, 
//                    classLoader
//            ); // Should print "hello".
//            /**
//             * Obtener la salida de la clase a ejecutar
//             */
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            PrintStream ps = new PrintStream(baos);
//            //restaurar el vijo out
//            PrintStream psold = System.out;
//            //Redireccionar la salida a la variable ps
//            System.setOut(ps);
//            //mandar a ejecutar la clase
//            Object instance = cls.newInstance();
//            /*
//            Una vez ejecutada, entonces guardar la salida en una 
//            variable String
//             */
//            out_code = new String(
//                    baos.toByteArray(), 
//                    StandardCharsets.UTF_8
//            );
//            /*
//            Regresar la salida a la normalidad
//             */
//            System.out.flush();
//            System.setOut(psold);

            return true;
        } catch (IOException ex) {
            Logger.getLogger(runtime.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(runtime.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(runtime.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
