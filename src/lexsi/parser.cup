/* Imports y packages */
package lexsi;

import java_cup.runtime.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java_cup.runtime.ComplexSymbolFactory.ComplexSymbol;

/* Codigo java */

 parser code {:

   /* Codigo personalizado para el parser
    */

    //Clase donde almaceno todo mi codigo
    lexsi.runtime rt = new lexsi.runtime();

    public void report_error(String message, Object info) {
        /*
        Al crear este método, automáticamente se le indica a jcup que dispare
        este método al detectar un error con este parser o con el código
        ingresado
        */
        rt.report_error(message, info); //Disparar el metodo report_error de 
                                        //mi clase generada
    }

    public void report_fatal_error(String message, Object info) {
        /*
            Cada vez que hay un error fatal del que jcup no se puede recuperar
        entonces se ejecuta este metodo atomaticamente.
        */
       	report_error(message, info);
    }

    public void syntax_error(Symbol cur_token){
        /*
     Éste método se dispara automaticamente cada que hay un error de sintaxis
        */
        report_error(
            "Error de syntaxis: "+ sym.terminalNames[cur_token.sym], null
        ); //Disparar al metodo report_error de arriba
    }

:}

/* 
    Nodos terminales. Son estos los que se ocupan para que el lexer de jflex
    retorne cada vez que detecta un token.
*/

terminal PROGRAM_T, IDENT, OP_ASIGNA, L_ENTERO, L_BOOLEAN, LLAVE_I;
terminal LLAVE_D, FUNCION_T, CONSTRUCTOR, PYCOMA, PRINT, COMPARE, PAR_L;
terminal COMA, STRING, BOOL, INT, CADENA, OP_BOOLEANO, IF, ELSE;
terminal C_FOR, OP_ARIT, SIGNO, COMILLAS, C_WHILE, PAR_D;

/* Nodos no terminales */

non terminal INICIO, program, cab_programa, cuerpo_prog, sent_asigna;
non terminal lit_str, funcion, cab_func, cuerpo_func, sent_comp, est_ctrl;
non terminal sentencia, call_func, func_print, func_compar, tipo_var, alt_sim;
non terminal est_alt, est_iter, est_ctrl_cuerpo, alt_dob, ciclo_for, cab_for;
non terminal exp_arit, iter_for, ciclo_while, cab_while, literal, expresion;
non terminal exp_arit_lit;

/* Sintaxis BNF */

start with INICIO;

INICIO ::= program:p;

program ::= cab_programa:cc cuerpo_prog:cp {: RESULT=rt.terminar(cp, cc); :};

cab_programa ::= PROGRAM_T IDENT:i {: rt.instObj(i); RESULT=i; :} ;

cuerpo_prog ::= LLAVE_I funcion:f LLAVE_D {: 
                                  RESULT=String.format("{\n\n\t %s\n\n} ", f); 
                                  :} ;

funcion ::= cab_func:fh cuerpo_func:fb 
            {: RESULT=String.format("%s%s", fh, fb); :} ;

cab_func ::= FUNCION_T CONSTRUCTOR PAR_L PAR_D 
            {: RESULT="\tpublic static void main(String[] args)"; :};

cuerpo_func ::= LLAVE_I sent_comp:sc LLAVE_D 
                    {: RESULT=rt.transCuerpoFunc(sc);:}
                | LLAVE_I LLAVE_D {: RESULT="{}"; :};

sent_comp ::= sent_comp:sc  sentencia:s 
                {: RESULT=String.format("%s\n%s",sc,s); :} 
            | sentencia:s {: RESULT=s; :} |
            error sentencia 
                {: report_error("Error en la sentencia", null); :};

sentencia ::= sent_asigna:sa PYCOMA 
                        {: RESULT=String.format("%s;", sa.toString() ); :} |
                error sent_asigna 
                        {: report_error("Error en la asignación", null); :} |
                 call_func:cf PYCOMA 
                    {: RESULT=String.format("%s;", cf.toString()); :} |
                error call_func 
                        {:  rt.report_error(
                                "Error en la llamada a la funcion", 
                                null); 
                        :}
                | est_ctrl:ec
                     {: RESULT=ec; :};

call_func ::= func_print:fp {: RESULT=fp; :} ;

func_print ::= PRINT PAR_L expresion:e PAR_D {: RESULT=rt.transPrint(e); :};

func_compar ::= COMPARE PAR_L expresion:ea COMA expresion:eb PAR_D 
                    {: RESULT=rt.transCompare(ea, eb, "=="); :}
                | COMPARE PAR_L expresion:ea COMA expresion:eb 
                    COMA OP_BOOLEANO:ob PAR_D 
                        {: RESULT=rt.transCompare(ea, eb, ob); :};
            
sent_asigna ::= tipo_var:tv IDENT:i OP_ASIGNA expresion:e 
                    {: RESULT=rt.transAsign(i, e, tv); :} | 
                IDENT:i OP_ASIGNA expresion:e 
                        {: RESULT=rt.transAsign(i, e); :};

est_ctrl ::= est_alt:ea {: RESULT=ea.toString(); :} 
                | est_iter:ei {: RESULT=ei; :};

est_alt ::= alt_sim:as {:RESULT=as.toString(); :} 
                | alt_dob:ad {: RESULT=ad; :};

est_iter ::= ciclo_for:cf {: RESULT=cf; :} 
            | ciclo_while:cw {: RESULT=cw; :};

alt_sim ::= IF PAR_L func_compar:fc PAR_D est_ctrl_cuerpo:ec 
                {: RESULT=String.format("if(%s)%s", fc, ec); :};

alt_dob ::= alt_sim:as ELSE est_ctrl_cuerpo:ec 
                {: RESULT=String.format("%selse%s", as, ec); :};

ciclo_for ::= cab_for:cf est_ctrl_cuerpo:ec 
                {: RESULT=String.format("%s%s", cf, ec); :};

ciclo_while ::= cab_while:cw est_ctrl_cuerpo:ec 
                {: RESULT=String.format(
                            "%s%s", 
                            cw.toString(), 
                            ec.toString()
                        ); 
                :};

cab_while ::= C_WHILE PAR_L func_compar:fc PAR_D 
                {: RESULT=String.format("while(%s)",fc.toString()); :};

cab_for ::= C_FOR PAR_L sent_asigna:sa PYCOMA func_compar:fc 
            PYCOMA iter_for:ea PAR_D 
                {: RESULT=String.format(
                            "for(%s;%s;%s)", 
                            sa.toString(), 
                            fc.toString(), 
                            ea.toString()
                        ); 
                :};

iter_for ::= IDENT:i SIGNO:sa SIGNO:sb 
                {: RESULT=String.format(
                            "%s%s%s", 
                            i.toString(), 
                            sa.toString(), 
                            sb.toString()
                        ); 
                :};

est_ctrl_cuerpo ::= LLAVE_I sent_comp:sc LLAVE_D 
                    {: RESULT=String.format("{\n\t%s\n}", sc.toString());; :} 
                    | LLAVE_I LLAVE_D {: RESULT="{\n\n}"; :};

expresion ::= literal:l {: RESULT=l; :} | 
                IDENT:i {: RESULT = rt.expresionIDFilter(i); :} |
                func_compar:cf {: RESULT = cf; :} |
                exp_arit:ea {: RESULT=ea; :};

exp_arit ::= exp_arit_lit:lea OP_ARIT:oa exp_arit_lit:leb  
                {: 
                    RESULT= rt.transExpArit(lea, oa, leb); 
                :};

exp_arit_lit ::= L_ENTERO:le  {: RESULT=le; :}
                | IDENT:i {: RESULT=i; :};

literal ::= lit_str:ls {: RESULT=ls; :} 
            | L_ENTERO:le {: RESULT=le; :}
            | L_BOOLEAN:lb {: RESULT=lb; :} ;

lit_str ::= CADENA:c {: RESULT=c; :};

tipo_var ::= STRING:s  {: RESULT=s; :} 
            | BOOL:lb {: RESULT=lb; :} 
            | INT:le {: RESULT=le; :} ;
