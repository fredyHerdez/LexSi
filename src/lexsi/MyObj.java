/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexsi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fredy
 */
public class MyObj {

    public MyObj(String name){
        nombre=name;
    }

	public String getName(){
		return nombre;
	}

    private final String nombre;

    private List<variable> variables;

    public variable setVar(Object name, Object value, Object type){
        if(variables==null){
            variables = new ArrayList<>();
        }
        String n = name.toString();
        if (
                    n.indexOf("\"") == 0
                    && n.substring(1).indexOf("\"") == (n.substring(1).length() - 1)
                    ){
            return null;
        }
        
        variable var = new variable(name);
        var.value=value;
        var.tipo = type;
        variables.add(var);
        return var;
    }
    
    
    public String getVarType(Object id){
        if(variables==null){
            return null;
        }
        for(variable v : variables){
            if(v.name.equals(id.toString())){
                return v.tipo.toString();
            }
        }
        
        try {
            Integer v = Integer.parseInt(id.toString());
            return "INT";
        } catch (NumberFormatException e) {
            String v = id.toString();
            if (
                    v.indexOf("\"") == 0
                    && v.substring(1).indexOf("\"") == (v.substring(1).length() - 1)
                    ){
                return "STRING";
            }else{
                if(v.equals("true") || v.equals("false")){
                    return "BOOL";
                }else if(
                        v.indexOf("(") == 0
                    && v.substring(1).indexOf(")") == (v.substring(1).length() - 1)
                        ){
                    return "INT";
                }
            }
        }
        return null;
    }
    
    public boolean updateVar(Object id, Object value){
        for(variable v : variables){
            if(v.name.equals(id.toString())){
                v.value = value;
                return true;
            }
        }
        return false;
    }
    
    public boolean isAritmeticExp(Object exp){
        String reg ="((+|-)?[0-9]+)(*|+|-|/)((+|-)?[0-9]+)";
        return false;
    }
    
    public boolean existsVar(Object n){
        if(variables==null)
            return false;
        for(variable v : variables){
            if(v.name.equals(n.toString())){
                return true;
            }
        }
        return false;
    }

	public variable getVar(String name){
		if(variables!=null){
			for(variable v : variables){
				if(v.name.equals(name)){
					return v;
				}
			}
		}
		return null;
	}

}
