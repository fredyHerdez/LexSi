/*Importaciones y paquetes*/
package lexsi;

import java_cup.runtime.*;

%%

/*
Opciones de la biblioteca,  codigo personalizado y declaración de macros.
*/

%class Lexer
%unicode
%cup
%line
%column
%eofval{
/*  A la variable msgLexer concatenamos EOF o fin del Archivo */
    return new Symbol(sym.EOF, null);
%eofval}
%{


    public int noTokens = 0, noErrores = 0;
    public String out;

    private void showMsg(String msj){
        if(out==null)
            out="LISTA DE TOKENS INGRESADOS:\n";
        out += "\t"+msj+"\n";
    }

    private Symbol Symb(int type, String s){
        showMsg(String.format(
                    "Token %d - %s\t->%s",
                    type, 
                    sym.terminalNames[type],s)
                );
        noTokens++;
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol Symb(int type, String s, Object value){
        String msj = s + "\t: " + value;
        Symb(type, msj);
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

MIN = [a-z]
MAY = [A-Z]
L=[a-zA-Z]
D = [0-9]
SIMB = "$"|"*"|"+"|"-"|"%"|"&"|"/"|"("|")"|"="|
        "?"|"!"|"¿"|"¡"|"."|","|";"|":"|"_"
WHITE=[ \t\r\n\b]
NUMBER = ("+"|"-")? {D}+
IDENT = {L}({L}|{D}|"_")*
CAR_VAL = {SIMB} | {L} | {NUMBER} | " "
CADENA = "\"" {CAR_VAL}* "\""

%%
/*
Declaraciones lexicas
*/

"if" { return Symb(sym.IF,"IF"); }
"(" { return Symb(sym.PAR_L,"("); }
")" { return Symb(sym.PAR_D,")"); }
"for" { return Symb(sym.C_FOR, "for"); }
"while" { return Symb(sym.C_WHILE,"while"); }
";" { return Symb(sym.PYCOMA,";"); }
"," { return Symb(sym.COMA,","); }
">" | "<" | "<=" | ">=" | "!=" | "==" { return Symb(
                                                sym.OP_BOOLEANO, 
                                                "OPERADOR BOOLEANO" , 
                                                yytext()
                                        );
                                        }
"\"" { return Symb(sym.COMILLAS,"\""); }
"TRUE" | "FALSE" { return Symb(sym.L_BOOLEAN, "BOOLEANO", yytext()); }
"+" | "-" { return Symb(sym.SIGNO, "SIGNO", yytext()); } 
"=" { return Symb(sym.OP_ASIGNA, "="); }
"PROGRAM" { return Symb(sym.PROGRAM_T, "PROGRAM"); }
"{" { return Symb(sym.LLAVE_I,"{"); }
"}" { return Symb(sym.LLAVE_D,"}"); }
"else" { return Symb(sym.ELSE,"ELSE"); }
"PRINT" { return Symb(sym.PRINT,"PRINT"); }
"COMPARE" { return Symb(sym.COMPARE,"COMPARE"); }
"STRING" { return Symb(sym.STRING,"STRING", yytext()); }
 "BOOL" { return Symb(sym.BOOL,"BOOL", yytext()); }
 "INT" { return Symb(sym.INT,"INT", yytext()); }
"INIT" { return Symb(sym.CONSTRUCTOR, "CONSTRUCTOR"); }
"FUNCION" { return Symb(sym.FUNCION_T, "FUNCION"); }
{NUMBER} { return Symb(sym.L_ENTERO, "ENTERO",yytext()); }
"-" | "+" | "*" | "/" { return Symb(
                                    sym.OP_ARIT, 
                                    "OPERADOR ARITMETICO", 
                                    yytext()
                                    ); }
{IDENT} { return Symb(sym.IDENT, "IDENTIFICADOR",yytext()); }
{CADENA} { return Symb(sym.CADENA, "CADENA", yytext()); }
{WHITE} {  /* ignorar */}

. { 
showMsg(String.format(
            "ERROR: Se ha detectado un error léxico en la linea %d, fila %d",
            yyline+1,
            yycolumn+1
        )
); 
}
