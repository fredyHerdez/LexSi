/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexsi;

/**
 *
 * @author ASUS
 */
public class Token {
//    id, white, digito, par_izq, par_der, pycoma, gbajo, guion, suma, resta, division,
//    multip, clase, cwhile, cfor, llavei, llaved, letra, esp, eif, punto;
    public static final int IF = 1;
    public static final int PAR_I = 2;
    public static final int PAR_D = 3;
    public static final int CFOR = 4;
    public static final int PYCOMA = 5;
    public static final int PUNTO = 6;
    public static final int COMA = 7;  
    public static final int L_BOOLEAN = 8;
    public static final int L_ENTERO = 9;
    public static final int MIN = 10;
    public static final int MAY = 11;
    public static final int DIGITO = 12;
    public static final int GBAJO = 13;
    public static final int GUION = 14;
    public static final int SIGNO = 15;
    public static final int SIMBOLO = 16;
    public static final int OP_BOOLEANO = 17;
    public static final int OP_ASIGNA = 18;
    public static final int ID = 19;
    public static final int OP_LOGICO = 20;
    public static final int OP_ARIT = 21;
    public static final int CWHILE = 22;
    public static final int CLASS = 23;
    public static final int LLAVE_I = 24;
    public static final int LLAVE_D = 25;
    public static final int ELSE = 26;
    public static final int PRINT = 26;
    public static final int COMPARE = 27;
    public static final int STRING = 28;
    public static final int BOOL = 29;
    public static final int ENTERO = 30;
    public static final int LETRA = 31;
    public static final int ERROR = 32;
    public static final int WHITE = 33;
    
    public static final String[][] tokens_dict = {
        {"if", "Condicional if"}, 
        {"Paréntesis izquierdo","Abrir paréntesis"},
        {"Paréntesis derecho", "Cerrar paréntesis"},
        {"for", "Iniciar ciclo for"},
        {"Punto y coma","Punto y coma"},
        {"Punto","Caracter punto"},
        {"Coma","Caracter coma"},
        {"Literal booleana", "Literal booleana (palabra reservada)"},
        {"Literal entero", "Literal entero"},
        {"Minúscula","Letra minúscula"},
        {"Mayúscula","Letra minúscula"},
        {"Dígito","Dígito"},
        {"Guion bajo","Guion bajo"},
        {"Guion","Guion"},
        {"Signo","Signo positivo/negativo"},
        {"Símbolo", "Símbolo"},
        {"Operador booleano","Operador booleano"},
        {"Operador de asignacion","Operador de asignación"},
        {"ID","Identificador"},
        {"Operarador lógico","Operador lógico"},
        {"while","Iniciar el ciclo while (palabra reservada)"},
        {"class","Declaracion de clase (palabra reservada)"},
        {"Llave izquierda","Inicio de bloque. Abrir llaves."},
        {"Llave derecha","Termino de bloque. Cerrar llaves."},
        {"else","Palabra reservada else. "},
        {"PRINT", "Palabra reservada para la función imprimir"},
        {"COMPARE", "Palabra reservada para la funcion comparar."},
        {"string","Asignar el tipo de dato a String"},
        {"bool","Asignar el tipo de dato a booleano"},
        {"int","Asignar el tipo de dato a entero"},
        {"letra", "Letra (mayúscula/minúscula)"},
        {"ERROR", "No se ha detectado token."},
        {"WHITE", "Espacio en blanco."},
    };
}
