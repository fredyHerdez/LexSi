/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexsi;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author fredy
 */
public class Fichero {
    
    //Leer, guardar y eliminar fichero codigo fuente, con ventanas y todo
    
    JFileChooser chooser;
    FileNameExtensionFilter fnef;
    String file_name;
    File fichero;
    public Reader reader;
    BufferedReader br;
    FileInputStream fis;
    
        
    public String open_file_box(Component c){
        /*
        Muestra el explorador de archivos para que el usuario elija el archivo
        a leer. Especificacndo solo para que lea archivos de texto con la 
        extension .fy.
        Parametros:
            Component: El componente raiz donde se va a mostrar la ventana
        Regresa:
            El codigo fuente en un String.
        */
        chooser = new JFileChooser();
        fnef = new FileNameExtensionFilter("Archivos fuente Fython",
                 "fy");
        chooser.setFileFilter(fnef);
        chooser.setCurrentDirectory(new File("/home/fredy")); //*****
        int resp = chooser.showOpenDialog(c);
        if(resp == JFileChooser.APPROVE_OPTION){
            try {
                String fte = readFile(chooser.getSelectedFile());
                chooser=null;
                fnef=null;
                return fte;
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(c, "Ha ocurrido un error mientras se abría el archivo.");
            }
        }
        return null;
    }
    
    
    public String readFile(File f) throws FileNotFoundException, IOException{
        /*
        Leer todo el stream de bytes del archivo elegido y convertirlo todo 
        a un String, para poder ser leido por los analizadores.
        Parametros:
            File f: El stream del archivo elegido.
        Regresa:
            El codigo fuente en un String
        */
        fichero = f;
        br = new BufferedReader(new FileReader(fichero));
        reader = new BufferedReader(new FileReader(fichero));
        fis = new FileInputStream(f);
        String texto = "";
        String ln;
        while((ln=br.readLine()) != null){
            texto += ln + "\n";
        }
        br.close();
        f=null;
        return texto;
    }
    
    public boolean save(String codigo, Component c){
        /**
         * Mostrar el explorador de archivos para que el usuario elija la 
         * ubicacion donde se va a guardar el codigo fuente.
         * Si es modificación de un archivo, entonces no se muestra el explorador
         * de archivos y se manda a guardar normal.
         * Paramteros:
         *      String codigo: El codigo fuente escrito en el programa.
         *      Component c: El componente donde se va a mostrar el explorador
         *          de archivos
         * Regresa:
         *      True o False dependiendo se el archivo se guardo o no.
         */
        
        
        /**
         * Verficiar si es archivo nuevo o no y despues guardar el archivo de
         * acuerdo a la situacion.
         * 
         */
        if(fichero!=null){
            try {
                reader = save_file(fichero, codigo);
                return true;
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(c, "Ha ocurrido un error mientras"
                        + " se guardaba el archivo.");
            }
        }else{
            // Mostrar el FileChooser para guardar el archivo
            return save_file_box(codigo, c);
        }
        return false;
    }
    
    public boolean save_file_box(String codigo, Component c){
        /**
         * Mostrar el chooser para guardar un archivo y, obviamente, mandarlo
         * a guardar.
         * Retornar true o false de acuerdo si se guardo o hubo un error.
         */
        // Configurar el chooser
        chooser = new JFileChooser();
        fnef = new FileNameExtensionFilter("TEXT FILES",
                "txt", "fy");
        chooser.setFileFilter(fnef);
        chooser.setCurrentDirectory(new File("/home/fredy"));
        int resp = chooser.showSaveDialog(c); //mostrar el chooser
        if(resp == JFileChooser.APPROVE_OPTION){
            try {
                /*
                Mandar a guardar y actualizar el buffered reader que se utiliza
                para JFlex.
                */
                reader = save_file(chooser.getSelectedFile(), codigo);
                chooser = null;
                fnef = null;
                return true;
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(c, "Ha ocurrido un error mientras"
                        + " se guardaba el archivo.");
            }
        }
        return false;
    }
    
    public BufferedReader save_file(File f, String cod) throws IOException{
        /**
         * Guardar el archivo en el disco y retornar el buffered del mismo
         * archivo actualizado.
         */
        FileWriter fw = new FileWriter(f);
        fw.write(cod);
        fw.close();
        return new BufferedReader(new FileReader(f));
    }
    
    
}
